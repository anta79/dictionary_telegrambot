### Used Modules
`botgram`
`node-fetch`


**Retrives search result(meanings of words) as an JSON array and shows them to users.**

### Demo
![](demo.gif)
